import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  updateCustomer(userId:string,id:string, name:string, education:number, income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        education:education,
        income:income,
        result:null,
      }
    )
  }

  updateResult(userId:string, id:string, result:string){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        result:result
      }
    )

  }

  deteleCustomer(userId:string ,id:string){
    this.db.doc(`users/${userId}/customers/${id}`).delete();
  }


  addCustomer(userId:string,name:string,education:number,income:number){
    const customer = {userId:userId,name:name,education:education,income:income}; 
    this.userCollection.doc(userId).collection('customers').add(customer);
  }
  
  public getCustomers(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`); 
    return this.customerCollection.snapshotChanges();
} 

  constructor(private db:AngularFirestore) { }
}
