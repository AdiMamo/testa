import { AuthService } from './../auth.service';
import { StudentService } from './../student.service';
import { PredictService } from './../predict.service';
import { Student } from './../interfaces/student';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  student:Student[];

  math: number;
  psychometric: number;
  options:Object[] = [
    {id:1,name:"pay"},
    {id:2,name:"not pay"}]
    option:string;
  pay;
  
  result:string;
  students$;
  email:string;


  add(){
    this.studentService.addStudent(this.email,this.math, this.psychometric, this.pay ,this.result); 
    this.router.navigate(['/students'])
  }

  cancel(){
    this.result = null;
  }

  predict(){
    // if(this.pay == "pay"){
    //   this.pay =0;
    // }else{
    //   this.pay =1;
    // }
    this.predictService.predict(this.math,this.psychometric,this.pay).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will Dropupt';
          console.log(result)
        } else {
          var result = 'Will not Dropout';
          console.log(result)
        }
        this.result = result
      }
    );  
  }
  
  constructor(public predictService:PredictService,private studentService:StudentService, private router:Router, public authService:AuthService) { }

  ngOnInit(): void {
     this.authService.getUser().subscribe(
        user => {
        this.email = user.email; 
        }
                )
  }
}
