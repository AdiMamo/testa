import { Component, OnInit } from '@angular/core';
import { Student } from '../interfaces/student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  displayedColumns: string[] = ['math', 'psychometric', 'pay', 'email', 'result', 'delete'];
  students$
  students:Student[];

  delete(id:string){
    this.studentService.deleteStudent(id);
  }

  constructor(private studentService:StudentService) { }

  ngOnInit(): void {
    this.students$ = this.studentService.getStudents();
    this.students$.subscribe(
      docs =>{
        this.students = [];
        for(let document of docs){
          const students:Student = document.payload.doc.data();
          students.id = document.payload.doc.id; 
          this.students.push(students); 
        }
      }
    ) 


  }

}
