import { CpredictService } from './../cpredict.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomerService } from '../customer.service';
import { Customers } from '../interfaces/customers';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  displayedColumns: string[] = ['name', 'education', 'income', 'delete', 'edit','predict','result'];

  rowToEdit:number = -1; 
  customerToEdit:Customers = {id:null,name:null, education:null, income:null};


  customers$;
  userId:string;
  customers = [];
  addCustomerFormOpen = false;
  result;
  saved:Boolean = false;


  predict(i){
    this.cpredictService.predict(this.customers[i].education,this.customers[i].income).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Yes';
          console.log(result)
        } else {
          var result = 'No';
          console.log(result)
        }
        this.customers[i].result = result      }
    );  
  }
  
  updateResult(index){
    this.customers[index].saved = true;
    this.customerService.updateResult(this.userId,this.customers[index].id,this.customers[index].result)
  }


  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.education = this.customers[index].education;
    this.customerToEdit.income = this.customers[index].income;
    this.rowToEdit = index; 
        }

  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.customerService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.education,this.customerToEdit.income);
    this.rowToEdit = null;
    }
    

  delete(id:string){
    this.customerService.deteleCustomer(this.userId,id);
  }


  add(customers:Customers){
    this.customerService.addCustomer(this.userId,customers.name,customers.education,customers.income); 
  }

  constructor(private customerService:CustomerService, public authService:AuthService, private cpredictService:CpredictService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.customers$ = this.customerService.getCustomers(this.userId);
        this.customers$.subscribe(
          docs =>{
            this.customers = [];
            for(let document of docs){
              const customer:any = document.payload.doc.data();
              if(customer.result){
                 customer.saved = true; 
                  }
              customer.id = document.payload.doc.id; 
              this.customers.push(customer); 
            }
          }
        ) 

        }
      )
  }
}

