import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Customers } from '../interfaces/customers';

@Component({
  selector: 'customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  @Input() id:string;
  @Input() name:string;
  @Input() education:number;
  @Input() income:number;
  @Input() customers =[];
  @Output() update = new EventEmitter<Customers>();
  @Output() closeForm = new EventEmitter<null>();
  isError:Boolean=false;

  tellToClose(){
    this.closeForm.emit();
  }

  updateParent(){
    let customers:Customers = {id:this.id, name:this.name, education:this.education, income:this.income};
    if(this.education<0 || this.education>24){
      this.isError=true;
    }else{
    this.update.emit(customers);
    this.name = null;
    this.education = null;
    this.income = null;
    this.isError = false;
  }
  }

  constructor() { }

  ngOnInit(): void {
  }

}
