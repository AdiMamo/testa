export interface Comments {
    userId:number;
    id:number;
    title:string;
    email:string;
    body:string;
}

