export interface Student {
    id?:string,
    math: number,
    psychometric: number,
    pay:boolean,    
    result?:string,
}
