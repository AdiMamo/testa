import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { Post } from '../interfaces/post';
import { PostService } from '../post.service';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {

  posts$:Observable<any>;
  userId:string;
  numLike:string;

  constructor(public auth:AuthService, private postService:PostService) { }

  Like(id:string, numLike:number){
    if(isNaN(numLike)){
      numLike = 1; 
    }else{
      numLike ++
     }
    this.postService.updateLike(this.userId,id,numLike)
    }

  deletePost(id:string){
    this.postService.deletePost(this.userId,id);
  }


  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user => {
      this.userId = user.uid;
      console.log(this.userId); 
      this.posts$ = this.postService.getPost(this.userId);        
        }
          ) 
        }
  }

