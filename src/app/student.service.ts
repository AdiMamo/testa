import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'


@Injectable({
  providedIn: 'root'
})
export class StudentService {

  studentCollection:AngularFirestoreCollection =  this.db.collection(`students`);

  public getStudents(){
    this.studentCollection = this.db.collection(`/students`); 
    return this.studentCollection.snapshotChanges();
} 

  deleteStudent(id:string){
    this.db.doc(`/students/${id}`).delete();
}

  addStudent(email:string,math:number, psychometric:number, pay:string, result:string){
    const student = {math:math, psychometric:psychometric,pay:pay,result:result,email:email};
    this.studentCollection.add(student);
  }


  constructor(private db:AngularFirestore) { }
}
