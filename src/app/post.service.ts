import { Comments } from './interfaces/comments';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from './interfaces/post';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  url = "https://jsonplaceholder.typicode.com/posts";
  urlComments = "https://jsonplaceholder.typicode.com/comments"

  postsCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');


  updateLike(userId:string, id:string, numLike:number){
      this.db.doc(`users/${userId}/posts/${id}`).update(
        {
          numLike:numLike
        }
      )
      }

  deletePost(Userid:string, id:string){
    this.db.doc(`users/${Userid}/posts/${id}`).delete(); 
      }

  getPost(userId){
    this.postsCollection = this.db.collection(`users/${userId}/posts`);
    return this.postsCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
              }
           )
          ))
        }


  addPost(userId:string,id:number,title:string,body:string){
    const posts = {id:id, title:title,body:body};
    this.userCollection.doc(userId).collection('posts').add(posts);
  }

  getPosts():Observable<Post>{
    return this.http.get<Post>(this.url)
    }

  getComments():Observable<Comments>{
    return this.http.get<Comments>(this.urlComments)
    }
        

  constructor(private http:HttpClient,private db:AngularFirestore) { }
}
