import { Comments } from './../interfaces/comments';
import { PostService } from './../post.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../interfaces/post';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-blog-posts',
  templateUrl: './blog-posts.component.html',
  styleUrls: ['./blog-posts.component.css']
})
export class BlogPostsComponent implements OnInit {

  posts$:Observable<Post>;
  comments$:Observable<Comments>;
  userId:string;
  saveState = [];

  add(id:number,title:string,body:string){
    this.postService.addPost(this.userId,id,title,body); 
  }

  constructor(private postService:PostService,public authService:AuthService) { }

  ngOnInit(): void {
    this.posts$ =  this.postService.getPosts();
    this.comments$ = this.postService.getComments();
    this.authService.user.subscribe(
      user => {
      this.userId = user.uid;
            }
          )
      
  }

}

