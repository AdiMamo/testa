import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogPostsComponent } from './blog-posts/blog-posts.component';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { CustomersComponent } from './customers/customers.component';
import { LoginComponent } from './login/login.component';
import { SavedPostsComponent } from './saved-posts/saved-posts.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { StudentFormComponent } from './student-form/student-form.component';
import { StudentsComponent } from './students/students.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'form', component: StudentFormComponent},
  { path: 'students', component: StudentsComponent},
  { path: 'customers', component: CustomersComponent},
  { path: 'customersform', component: BlogPostsComponent},
  { path: 'posts', component: BlogPostsComponent},
  { path: 'saved', component: SavedPostsComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
