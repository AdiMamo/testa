// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAMW_DZ2bdjT4izOBxYn32xzGVUl0nAtZU",
    authDomain: "testa-56963.firebaseapp.com",
    projectId: "testa-56963",
    storageBucket: "testa-56963.appspot.com",
    messagingSenderId: "371672118497",
    appId: "1:371672118497:web:b18d14172a1754ba2073a0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
